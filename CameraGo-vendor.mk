PRODUCT_SOONG_NAMESPACES += \
    vendor/CameraGo

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/CameraGo/proprietary/system/etc,$(TARGET_COPY_OUT_SYSTEM)/etc)

PRODUCT_PACKAGES += \
    CameraGo
